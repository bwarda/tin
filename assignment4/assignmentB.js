//Task b
function fibonacci(num) {
  let sol = [0,1];
  for (let i = 2; i <= num; i++) {
    sol.push(sol[i-1] + sol[i-2]);    
  }
  return sol.pop();
}
console.log('Fibonacci: '+fibonacci(17));