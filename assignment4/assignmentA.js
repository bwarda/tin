//Task a
//Recursively using function expression
var fun = function factorialRecur(n){
    if (n === 0) { 
    	return 1; 
    } else { 
    	return n * factorialRecur(n - 1); 
    }
}
console.log('Factorial recursively using function expression: '+fun(6));
//Iteratively using function declaration
function factorialIter(n){
	var sol=1;
	for (var i = 2; i <= n; i++){
  sol = sol * i;
  }      
  return sol;
}
console.log('Factorial iteratively using function declaration: '+factorialIter(6));