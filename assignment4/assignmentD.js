//Task d
function makeAlphabet(s) { 
	var d = s.split('');
  var regex = '/\s+/g';
  var alph = d.sort().join('').replace(regex, '');
	return alph; 
}
console.log('Alphabetic sort : '+(makeAlphabet("dacb")));