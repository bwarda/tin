//Task i
function convertMoneyToCoins(money, arr) {
  var coins = [];
  for (var i = 0; i < arr.length; i++) {
    while (money >= arr[i]) {
      coins.push(arr[i]);
      money = money - arr[i];
    }
  }

  return coins;
}
console.log('Converting money to coins: '+convertMoneyToCoins(46, [100, 50, 20, 5, 2,1]));