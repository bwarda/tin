//Task g
function guessDataType(val){
  var dataTypes = [Function, RegExp, Number, String, Boolean, Object];
  var i;
  var l;    
  if (typeof val === "function" || typeof val === "object") {
		for (i = 0, l = dataTypes.length; i < l; i++) {
			if (val instanceof dataTypes[i]) {
				return dataTypes[i];
      }
    }
  }
  return typeof val;
}
console.log('Guess data type2: '+guessDataType('jestem stringiem'));
console.log('Guess data type1: '+guessDataType(89));
console.log('Guess data type3: '+guessDataType(true));