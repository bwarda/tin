//Task c
function palindrome(s) {
	var l = s.length;
 	let regex = /[^A-Za-z0-9]/g;
 	s = s.toLowerCase().replace(regex, '');
 	for (var i = 0; i < l/2; i++) {
   	if (s[i] !== s[l - 1 - i]) {
   		return false;
 		}
	}
 	return true;
}
console.log('Palindrome: '+palindrome("kajak"));