//Task j
function binarySearch(arr, targetNum) {    
    let right = arr.length - 1;
    let left = 0;
    while (right >= left) {
        var half = Math.floor((right - left) / 2);
        const middle = left + half;
        if (arr[middle] === targetNum) {
            return middle;
        }
        if (arr[middle] < targetNum) {
            left = middle + 1;
        } else {
            right = middle - 1;
        }
    }
    return -1;
}
console.log('Binary search index number of the targeted number: ' + binarySearch([6,3,5,4,2,9,7,5,6],7));
