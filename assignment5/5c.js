//Task 5c
function Student(firstName, lastName, id) {
	this.firstName = firstName;
	this.lastName = lastName;
	this.id = id;	
}
Student.prototype.courses = {courses: ['BYT','GRK','SAD','WF','TIN','PRO','JAP']};

var hh = new Student('Ned','Donkey',13543);
console.log(hh.courses);