//Task 5f
class Person{
	constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName= lastName;
  }
  
  get fullName(){
  	return this.firstName+" "+this.lastName;
  }
  
  set fullName(name) {
  	var fullName = name.split(" ");
    this.firstName = fullName[0];
    this.lastName = fullName[1];
  }
}

class Student extends Person {
	constructor(firstName, lastName, id, grades) {
  	super(firstName, lastName);
  	this.id = id;
  	this.grades = grades;
	}
}

let p = new Person('Tom', 'Hanks');
let stud = new Student('kill', 'bill',13432, [2,4,5,3,2]);
console.log(p);
console.log(stud);
console.info("Student's grades: "+stud.grades);