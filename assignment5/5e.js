//Task 5e
class Student{
	constructor(firstName, lastName, id, grades) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
    this.grades = grades;
	}
	
  get avarage(){
  	var grades = this.grades;
		var sum = 0;
		for(var i = 0; i < grades.length; i++){
			sum += grades[i];
		}
  	var avg = sum/grades.length;
  	return avg;
  }
  
  get fullName(){
  	return this.firstName+" "+this.lastName;
  }
  
  set fullName(name) {
  	var fullName = name.split(" ");
    this.firstName = fullName[0];
    this.lastName = fullName[1];
  }
}

var st1 = new Student("Ben","TinHead",13458,[1,3,4,5,2,3]);
console.log("avg: "+st1.avarage);
console.log("get: "+st1.fullName);
console.log("set: "+(st1.fullname = 'Cris Loop'));