//Task 5a
var person = {name: 'Bob', surname: 'Bear', id: 13964, 
							greeting: function greeting(){console.log('Hello, my name is '+person.name+" "+person.surname)},
              sum: function sum(a, b){console.log(a+" + "+b+" = "+(a+b))}            
              };

function getType(value) {
	var nameAndType = "";
  var x;
	for (x in person) {
 		nameAndType += person[x]+"'s type is: "+ typeof person[x]+"\n";
	}
  console.log(nameAndType);
}


console.log("person's id: "+person.id);
person.greeting();
person.sum(3,6);
getType(person);