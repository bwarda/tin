//Task 5d
function Student(firstName, lastName, id, grades) {
	this.firstName = firstName;
	this.lastName = lastName;
	this.id = id;
	this.grades = grades;
}

Student.prototype = {
	get avarage(){
  	var grades = this.grades;
		var sum = 0;
		for(var i = 0; i < grades.length; i++){
			sum += grades[i];
		}
  	var avg = sum/grades.length;
  	return avg;
  },
	
  get fullName() {
  	return this.firstName+" "+this.lastName;
  },
  
  set fullName(name) {
    var names = name.split(" ");
    this.firstName = names[0];
    this.lastName = names[1];
	}	
}
var st1 = new Student("Ben","TinHead",13458,[1,3,4,5,2,3]);

console.log("get: "+st1.fullName);
console.dir("set: "+(st1.fullname ='Ned Clash'));
console.log("avg: "+st1.avarage);