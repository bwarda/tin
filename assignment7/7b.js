var fs = require("fs");
var path = process.argv[2];
dirWatcher(path);

function dirWatcher(path) {
    fs.watch(path, (eventType, fileName) => {
        console.log(fileName + " " + eventType);
        if (eventType === "change") {
			fs.readFile(path + fileName, "utf-8", (err, data) => {
			if (err) {
				console.log(err);
				return;
			}
			console.log(data);
		});
        }
    });
}