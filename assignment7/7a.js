const http = require('http');
const url = require('url');

let server = http.createServer((req, res) => {
	let parsedUrl = url.parse(req.url, true);
	let query = parsedUrl.query;

	let a = parseInt(query.a);
	let b = parseInt(query.b);
	  
	let address = req.url;
	
	if (address.includes('add')) {
	 operator(a, b, address, res);
    }
	else if (address.includes('sub')) {
	 operator(a, b, address, res);
    }
	else if (address.includes('mul')) {
	operator(a, b, address, res);
    }
	else if (address.includes('div')) {
	 operator(a, b, address, res);
    } 
	else {
		res.end("Invalid operators");
	}
});

function operator(num1, num2, operation, outcome) {	
	if(isNaN(num1) || isNaN(num2) === false) {
	  if (operation.includes("add")) {
		 outcome.write(num1 + num2+'');
		 outcome.end();
	  }
	  if (operation.includes("sub")) {
		 outcome.write(num1 - num2+'');
		 outcome.end();
	  } 
	  if (operation.includes("mul")) {
		 outcome.write(''+num1 * num2);
		 outcome.end();
	  }
	  if (operation.includes("div")) {
		 outcome.write(''+num1 / num2);
		 outcome.end();
	  }
	} else {
		outcome.end("Included parameters are not numbers. Please change that.");
	}		
}

server.listen(1337);

console.log("Server started!");
console.log("Run localhost:1337/(operator)?a=(number1)&b=(number2)");
console.log("For instance: http://localhost:1337/sub?a=2&b=8");
console.log("Operations: add, sub, mul, div");